# 后台管理

#### 介绍
	基于python3编写的web后台管理系统,有任何个性化需求请加群联系作者.

#### 软件架构
	本项目基于python3语言开发，后台框架使用layui，并使用了一些基于layui的第三方插件、pyecharts等开源插件进行开发。

#### 使用说明
	1.  本项目基于python3语言开发，后台框架使用layui，并使用了一些基于layui的第三方插件。
	2.  本项目属于私有项目，未经允许，严谨售卖，否则后果自负。
	3.  欢迎大家提出自己的优化建议、系统的BUG，我会及时进行修复。
	4.  由于服务器是远程服务器，因此没有可用的摄像头，无法使用人脸识别登录。
	5.  开源安装包只包含演示系统中部分功能，要获取更多功能请联系作者。

#### 系统介绍
	1.  系统包含账号登录和人脸识别登录。
	2.  系统可自定义登录页背景图片。
	3.  系统包含了机构权限、数据权限、菜单权限三大模块。
	4.  系统包含用户问题反馈功能，便于运维人员进行线上问题处理。
	5.  系统包含消息推送以及个人资料修改、头像上传功能。
	6.  系统包含审批流程功能，可视化的流程配置。
	7.  系统包含线上问题反馈功能，可实时进行沟通。

#### 开发周期
	1. 2021.06.10 更新了机构数的数据组装，使用treelib解决了机构数层级限制问题。
	2. 2021.06.18 解决layui表格筛选勾选隐藏列后导致隐藏列在表格中展示
	3. 2021.06.24 调整了部门管理页面有滚动条的问题
	4. 2021.07.08 优化了页面数据展示权限，无权限时跳转指定页面
	5. 2021.07.14 给登录页面账户名称前添加了头像和子菜单
	6. 2021.07.16 添加了个人资料与密码修改，同时增加了表字段
	7. 2021.07.20 优化了角色管理部分页面
	8. 2021.07.27 添加了数据字典功能
	9. 2021.07.26 优化菜单管理在添加菜单时通过页面按钮初始化菜单权限
	10. 2021.07.30 添加了管辖范围限制，当前机构的人员只能查询当前机构以及下属机构的数据
	11. 2021.08.02 给角色管理添加了数据过滤，仅展示当前用户权限范围内的数据
	12. 2021.08.05 完善了左上角了个人消息功能
	13. 2021.08.09 完善左上角消息图标添加数量提示
	14. 2021.08.16 新增了自定义桌面壁纸功能
	15. 2021.08.21 新增了桌面背景菜单中图片点击预览功能，同时新增了系统版本字典项
	16. 2021.09.14 新增了问题反馈功能
	17. 2021.11.8 首页添加了echarts图表
	17. 2021.11.11 新增登录窗口自定义功能
	18. 2021.11.11 新添加了一个动态背景
	19. 2021-11-21 在人物头像下添加了机构切换功能
	20. 2021-11-25 日志记录添增加IP记录和登录地点记录
	20. 2021-11-29 个人消息添加了批量阅读和删除功能
	21. 2021-12-20 OA审批工作流开发
	22. 2022-02-18 系统公告功能开发
	23. 2022-03-25 ‘公告管理’和‘公告图文’功能开发
	24. 2022-04-08 二维码在线生成功能开发
	25. 2022-06-22 添加了tab右键菜单功能
	26. 2022-06-30 优化了tab右键菜单BUG
	27. 2022-07-05 完成了MYSQL数据库自动备份功能
	28. 2022-07-06 完成备份还原功能，同时优化了数据库自动备份功能
	29. 2022-09-19 修复菜单管理中排序混乱的问提
	30. 2022-11-12 演示系统新增功能数据配置、图表配置
	31. 2022-11-23 演示系统新增表格配置新功能
	32. 2023-06-15 图标配置中新增缩略图自动生成功能
	33. 2023-09-04 优化了审批流，开发审批历史记录
	34. 2023-09-15 优化了标准流程审批页面，添加审批进度视图
	35. 2023-10-16 完成变更类审批流程的开发，实现了变更前后数据变化对比
	36. 2023-10-31 完成了系统首页的开发，展示系统真实数据。
	37. 2023-11-20 完成了工作管理、我的工作菜单开发。
	38. 2023-12-04 完成了流程图中判断节点的改造，判断节点可分支为多条不同的审批路线。
	39. 2024-01-31 完成了视频管理功能、在后台进行视频格式转换，对视频进行分割。
	40. 2024-04-07 新增了模拟点位、组态点位菜单、同时组态可进行点位绑定。

#### 更新记录
	1. 2021-11-02 上传了源代码
	2. 2021-11-05 上传了桌面背景菜单自定义设置功能相关源代码
	3. 2021-11-08 上传了问题反馈功能功能相关源代码
	4. 2021-11-11 上传了登录窗口自定义功能相关源代码
	5. 2021-11-11 处理了linux系统下菜单图标展示异常的问题
	6. 2021-11-12 对窗口设置进行了优化
	7. 2021-11-15 添加了动态背景
	8. 2021-11-15 对系统进行了优化，修复已知BUG
	9. 2021-11-22 在人物头像下新增机构切换功能
	10. 2021-11-26 日志记录添增加IP记录和登录地点记录
	11. 2021-11-29 个人消息添加了批量阅读和删除功能
	12. 2021-12-21 OA审批流程更新
	13. 2021-12-21 O系统公告功能更新
	14. 2022-03-25 更新菜单‘公告管理’和‘公告图文’
	15. 2022-06-02 修复问题反馈菜单下的已知BUG
	16. 2022-06-23 添加了tab右键菜单功能
	17. 2022-07-05 添加了数据库自动备份功能
	18. 2022-07-06 更新备份还原功能，同时优化了数据库自动备份功能
	19. 2022-09-19 修复菜单管理中排序混乱的问提
	20. 2022-11-12 演示系统更新了菜单数据配置、图表配置
	21. 2022-11-15 对演示系统中数据配置、图表配置功能进行了优化
	22. 2022-11-23 对演示系统中图表配置功能进行了优化
	23. 2022-11-24 优化了表格配置功能，添加了‘合计’行
	24. 2024-09-08 更改流程查看详情页面，添加了审批历史记录
	25. 2023-10-16 更新常规审批流程页面优化，新增变更类审批流程
	26. 2023-10-31 更新了系统首页。
	27. 2023-11-24 更新了工作管理相关菜单。
	28. 2023-12-05 更新了工作流判断改造相关功能。
	29. 2024-01-31 更新了视频管理功能，对视频进行格式转换、视频分割、截图操作。
	30. 2024-04-07 更新了组态配置、组态视图，可动态展示点位数据。

#### python3技术交流群
![Image text](https://gitee.com/XueHuaPiaoPiaoYiZhenFeng/back-stage-management/raw/master/qr_code/20211203154745.png)

#### 联系作者
	群号：709378856
	VX:syj57606873
	有任何问题可随时联系群主。

#### 作者留言
	1.  欢迎大家对系统中存在的BUG积极提出。
	2.  欢迎大家提出新的需求。
	3.  为了防止有些朋友在系统中胡乱操作，演示系统已禁用部分编辑权限。
	4.  源码中只包含基本的后台管理功能菜单，和演示系统功能菜单可能对不上。
	5.  该系统将持续进行更新，对大家提出的问题也会及时进行答复。
	6.  有任何问题可扫描二维码加群私聊。
	7.  有任何个性化需求请加群联系作者。

# 演示系统地址
#### 《 后台管理》演示地址：http://www.xuehuapiaopiaoyizhenfeng.asia
	登录账号：user1 密码：123123

#### 《 资源监控系统》源码下载地址：
https://gitee.com/XueHuaPiaoPiaoYiZhenFeng/monitor

# 系统截图
#### 登录界面
![Image text](https://gitee.com/XueHuaPiaoPiaoYiZhenFeng/back-stage-management/raw/master/qr_code/首页.png)

#### 系统首页
![Image text](https://gitee.com/XueHuaPiaoPiaoYiZhenFeng/back-stage-management/raw/master/qr_code/系统首页.png)

#### 个人中心界面
![Image text](https://gitee.com/XueHuaPiaoPiaoYiZhenFeng/back-stage-management/raw/master/qr_code/%E4%B8%AA%E4%BA%BA%E8%B5%84%E6%96%99.png)
![Image text](https://gitee.com/XueHuaPiaoPiaoYiZhenFeng/back-stage-management/raw/master/qr_code/%E6%9C%BA%E6%9E%84%E5%88%87%E6%8D%A2.png)
![Image text](https://gitee.com/XueHuaPiaoPiaoYiZhenFeng/back-stage-management/raw/master/qr_code/%E6%B6%88%E6%81%AF%E4%B8%AD%E5%BF%83.png)

#### 系统设置界面
![Image text](https://gitee.com/XueHuaPiaoPiaoYiZhenFeng/back-stage-management/raw/master/qr_code/%E8%8F%9C%E5%8D%95.png)
![Image text](https://gitee.com/XueHuaPiaoPiaoYiZhenFeng/back-stage-management/raw/master/qr_code/%E9%83%A8%E9%97%A8.png)
![Image text](https://gitee.com/XueHuaPiaoPiaoYiZhenFeng/back-stage-management/raw/master/qr_code/%E4%B8%AA%E4%BA%BA%E8%AE%BE%E7%BD%AE.png)
![Image text](https://gitee.com/XueHuaPiaoPiaoYiZhenFeng/back-stage-management/raw/master/qr_code/%E6%9D%83%E9%99%90.png)
![Image text](https://gitee.com/XueHuaPiaoPiaoYiZhenFeng/back-stage-management/raw/master/qr_code/%E7%94%A8%E6%88%B7.png)

#### 登录页面背景自定义
![Image text](https://gitee.com/XueHuaPiaoPiaoYiZhenFeng/back-stage-management/raw/master/qr_code/%E6%A1%8C%E9%9D%A2.png)
![Image text](https://gitee.com/XueHuaPiaoPiaoYiZhenFeng/back-stage-management/raw/master/qr_code/%E7%AA%97%E5%8F%A3.png)

#### 审批流配置
![Image text](https://gitee.com/XueHuaPiaoPiaoYiZhenFeng/back-stage-management/raw/master/qr_code/%E6%B5%81%E7%A8%8B%E7%AE%A1%E7%90%86.png)
![Image text](https://gitee.com/XueHuaPiaoPiaoYiZhenFeng/back-stage-management/raw/master/qr_code/%E6%B5%81%E7%A8%8B%E5%9B%BE.png)
![Image text](https://gitee.com/XueHuaPiaoPiaoYiZhenFeng/back-stage-management/raw/master/qr_code/%E8%8A%82%E7%82%B9%E9%85%8D%E7%BD%AE.png)
![Image text](https://gitee.com/XueHuaPiaoPiaoYiZhenFeng/back-stage-management/raw/master/qr_code/%E8%B7%AF%E7%BA%BF%E9%85%8D%E7%BD%AE.png)
![Image text](https://gitee.com/XueHuaPiaoPiaoYiZhenFeng/back-stage-management/raw/master/qr_code/%E5%AE%A1%E6%89%B9%E8%AF%A6%E6%83%85.png)

#### 标准流程
![Image text](https://gitee.com/XueHuaPiaoPiaoYiZhenFeng/back-stage-management/raw/master/qr_code/%E6%A0%87%E5%87%86%E6%B5%81%E7%A8%8B%E5%AE%A1%E6%89%B9%E9%A1%B5%E9%9D%A2.png)

#### 变更类流程
![Image text](https://gitee.com/XueHuaPiaoPiaoYiZhenFeng/back-stage-management/raw/master/qr_code/%E5%8F%98%E6%9B%B4%E7%94%B3%E8%AF%B7%E4%B8%BB%E9%A1%B5%E9%9D%A2.png)
![Image text](https://gitee.com/XueHuaPiaoPiaoYiZhenFeng/back-stage-management/raw/master/qr_code/%E5%8F%98%E6%9B%B4%E6%B5%81%E7%A8%8B%E7%94%B3%E8%AF%B7%E9%A1%B5%E9%9D%A2.png)

#### 变更流程审批
![Image text](https://gitee.com/XueHuaPiaoPiaoYiZhenFeng/back-stage-management/raw/master/qr_code/%E5%8F%98%E6%9B%B4%E6%B5%81%E7%A8%8B%E5%AE%A1%E6%89%B9%E9%A1%B5%E9%9D%A2.png)

#### 图表可视化
![Image text](https://gitee.com/XueHuaPiaoPiaoYiZhenFeng/back-stage-management/raw/master/qr_code/%E6%95%B0%E6%8D%AE%E9%85%8D%E7%BD%AE.png)
![Image text](https://gitee.com/XueHuaPiaoPiaoYiZhenFeng/back-stage-management/raw/master/qr_code/%E6%95%B0%E6%8D%AE%E8%A1%A8%E9%85%8D%E7%BD%AE.png)
![Image text](https://gitee.com/XueHuaPiaoPiaoYiZhenFeng/back-stage-management/raw/master/qr_code/%E5%85%B3%E8%81%94%E5%85%B3%E7%B3%BB%E9%85%8D%E7%BD%AE.png)
![Image text](https://gitee.com/XueHuaPiaoPiaoYiZhenFeng/back-stage-management/raw/master/qr_code/%E5%9B%BE%E8%A1%A8%E9%85%8D%E7%BD%AE.png)
![Image text](https://gitee.com/XueHuaPiaoPiaoYiZhenFeng/back-stage-management/raw/master/qr_code/%E8%A1%A8%E6%A0%BC%E9%85%8D%E7%BD%AE.png)

#### 工作管理
![Image text](https://gitee.com/XueHuaPiaoPiaoYiZhenFeng/back-stage-management/raw/master/qr_code/%E5%B7%A5%E4%BD%9C%E5%AE%89%E6%8E%92.png)
![Image text](https://gitee.com/XueHuaPiaoPiaoYiZhenFeng/back-stage-management/raw/master/qr_code/%E5%8F%91%E5%B8%83%E5%B7%A5%E4%BD%9C.png)
![Image text](https://gitee.com/XueHuaPiaoPiaoYiZhenFeng/back-stage-management/raw/master/qr_code/%E6%88%91%E7%9A%84%E5%B7%A5%E4%BD%9C.png)
![Image text](https://gitee.com/XueHuaPiaoPiaoYiZhenFeng/back-stage-management/raw/master/qr_code/%E5%B7%A5%E4%BD%9C%E8%AF%A6%E6%83%85.png)

#### 视频管理
![Image text](https://gitee.com/XueHuaPiaoPiaoYiZhenFeng/back-stage-management/raw/master/qr_code/%E8%A7%86%E9%A2%91%E5%A4%84%E7%90%86.png)
![Image text](https://gitee.com/XueHuaPiaoPiaoYiZhenFeng/back-stage-management/raw/master/qr_code/%E8%A7%86%E9%A2%91%E8%AF%A6%E6%83%85.png)

#### 数据库自动备份还原
![Image text](https://gitee.com/XueHuaPiaoPiaoYiZhenFeng/back-stage-management/raw/master/qr_code/%E5%A4%87%E4%BB%BD%E8%AE%BE%E7%BD%AE.png)
![Image text](https://gitee.com/XueHuaPiaoPiaoYiZhenFeng/back-stage-management/raw/master/qr_code/%E5%A4%87%E4%BB%BD%E8%BF%98%E5%8E%9F.png)

#### 工业组态
![Image text](https://gitee.com/XueHuaPiaoPiaoYiZhenFeng/back-stage-management/raw/master/qr_code/%E6%A8%A1%E6%8B%9F%E7%82%B9%E4%BD%8D.png)
![Image text](https://gitee.com/XueHuaPiaoPiaoYiZhenFeng/back-stage-management/raw/master/qr_code/%E7%82%B9%E4%BD%8D%E6%8C%82%E8%BD%BD.png)
![Image text](https://gitee.com/XueHuaPiaoPiaoYiZhenFeng/back-stage-management/raw/master/qr_code/%E5%91%8A%E8%AD%A6%E9%85%8D%E7%BD%AE.png)
![Image text](https://gitee.com/XueHuaPiaoPiaoYiZhenFeng/back-stage-management/raw/master/qr_code/%E7%BB%84%E6%80%81%E7%82%B9%E4%BD%8D.png)
![Image text](https://gitee.com/XueHuaPiaoPiaoYiZhenFeng/back-stage-management/raw/master/qr_code/%E7%BB%84%E6%80%81%E9%85%8D%E7%BD%AE.png)
![Image text](https://gitee.com/XueHuaPiaoPiaoYiZhenFeng/back-stage-management/raw/master/qr_code/%E7%BB%84%E6%80%81%E8%A7%86%E5%9B%BE.png)


#### 问题反馈界面
![Image text](https://gitee.com/XueHuaPiaoPiaoYiZhenFeng/back-stage-management/raw/master/qr_code/%E5%85%B6%E4%BB%96%E5%8A%9F%E8%83%BD.png)

#### 系统公告界面
![Image text](https://gitee.com/XueHuaPiaoPiaoYiZhenFeng/back-stage-management/raw/master/qr_code/%E5%85%AC%E5%91%8A%E8%AE%BE%E7%BD%AE.png)
![Image text](https://gitee.com/XueHuaPiaoPiaoYiZhenFeng/back-stage-management/raw/master/qr_code/%E5%85%AC%E5%91%8A%E5%9B%BE%E7%89%87.png)
![Image text](https://gitee.com/XueHuaPiaoPiaoYiZhenFeng/back-stage-management/raw/master/qr_code/%E5%85%AC%E5%91%8A%E5%B1%95%E7%A4%BA.png)

#### 爬虫示例
![Image text](https://gitee.com/XueHuaPiaoPiaoYiZhenFeng/back-stage-management/raw/master/qr_code/%E7%88%AC%E8%99%AB%E7%A4%BA%E4%BE%8B.png)

#### 二维码生成页面
![Image text](https://gitee.com/XueHuaPiaoPiaoYiZhenFeng/back-stage-management/raw/master/qr_code/%E4%BA%8C%E7%BB%B4%E7%A0%81%E7%94%9F%E6%88%90.png)

#### 操作日志记录
![Image text](https://gitee.com/XueHuaPiaoPiaoYiZhenFeng/back-stage-management/raw/master/qr_code/%E6%97%A5%E5%BF%97%E7%AE%A1%E7%90%86.png)